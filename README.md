# Cyberwoven Paragraphs - Duo WYSIWYG Layer

## CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers
  * TSD Snippet


## INTRODUCTION
---------------------

A Paragraph bundle made by Cyberwoven.

For content creators, attempts to use wysiwyg editors to create structured
layouts typically lead to frustration and compromise.


## REQUIREMENTS
---------------------

This module requires the following modules:

  * [Paragraphs](https://www.drupal.org/project/paragraphs)


## INSTALLATION
---------------------

  In your composer.json:

``` json
  {
    "repositories": [
      {
        "type": "package",
        "package": {
          "name": "cyberwoven/cwp-duo-wysiwyg-layer",
          "version": "0.1.3",
          "type": "drupal-module",
          "dist": {
            "url": "https://bitbucket.org/cyberwoven/cwp-duo-wysiwyg-layer/get/0.1.3.zip",
            "type": "zip"
          },
          "source": {
            "url": "git@bitbucket.org:cyberwoven/cwp-duo-wysiwyg-layer.git",
            "type": "git",
            "reference": "0.1.3"
          }
        }
      },
    ],
    "require": {
      "cyberwoven/cwp-duo-wysiwyg-layer": "v0.1.3",
    }
  }
```

  * Make sure `version`, `reference` match the same tag/version in `require` *
  * The tag is the name of the zip folder. *
  * Pattern: `https://bitbucket.org/cyberwoven/<repo_name>/get/<tag_name>.zip` *
  * Install the module as you normally would.
  * Verify installation by visiting /admin/structure/paragraphs_type and seeing
  your new Paragraph bundles.


## CONFIGURATION
---------------------

  * Go to your content type and add a new field of type Entity revisions,
    Paragraphs.
  * Allow unlimited so creators can add more that one Paragraph to the node.
  * On the field edit screen, you can add instructions, and choose which
    bundles you want to allow for this field.
  * Arrange them as you see fit.
  * Adjust your form display, placing the field where you want it.
  * Add the field into the Manage display tab.
  * Start creating content!

## THEMING
---------------------

There are styles in `scss` format to use as a base/jump-off-point in `scss/cwp-duo-wysiwyg-layer.scss`.


## DEVELOPMENT/UPDATES
---------------------

**Tip: Use the script `clean-configs` to remove `UUID` and `config_hash` from exported configs.**


## MAINTAINERS
---------------------

Current maintainers:
  * [Tai Vu](https://www.drupal.org/u/taivu)

This project has been sponsored by:
  * [Cyberwoven](https://www.cyberwoven.com)

Inspired by:
  * [Bootstrap Paragraphs](https://www.drupal.org/project/bootstrap_paragraphs)


## TSD Snippet

``` md
### Duo WYSIWYG Layer
  * Main Content (WYSIWYG, optional)
  * Aside Content (WYSIWYG, optional)
  * Checkbox: Enable Aside Content (boolean field, optional)
  * Checkbox: Switch order of content/image (boolean field, optional)
  * Radio Checkbox: Content Sizing (Main Larger, Aside Larger, or even 50/50) (list field, required, default Main Larger. hidden unless Enable Aside Content is enabled)
```
