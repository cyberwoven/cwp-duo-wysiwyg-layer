<?php

/**
 * @file
 * Bootstrap Paragraphs module file.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_theme().
 */
function cwp_duo_wysiwyg_layer_theme($existing, $type, $theme, $path) {
  return [
    'paragraph__default' => [
      'base hook' => 'paragraph',
    ],
    'paragraph__cwp_duo_wysiwyg_layer' => [
      'base hook' => 'paragraph',
    ],
  ];
}

/**
 * Implements hook_help().
 *
 * @inheritdoc
 */
function cwp_duo_wysiwyg_layer_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.cwp_duo_wysiwyg_layer':
      $text = file_get_contents(dirname(__FILE__) . "/README.md");
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}


/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 * 
 * Widget : Paragraphs Classic : entity_reference_paragraphs
 * 
 */
function cwp_duo_wysiwyg_layer_field_widget_entity_reference_paragraphs_form_alter(&$element, &$form_state, $context) {
  if ($element['#paragraph_type'] != 'cwp_duo_wysiwyg_layer') {
    return;
  }
  // $field_name_to_hide = 'cwp_dwl_content_aside';

  _cwp_duo_wysiwyg_layer_paragraph_field_state($element, 'cwpf_content_aside', 'cwpf_enable_aside_content', 'visible', ["checked" => true]);
  _cwp_duo_wysiwyg_layer_paragraph_field_state($element, 'cwpf_content_sizing', 'cwpf_enable_aside_content', 'visible', ["checked" => true]);
  _cwp_duo_wysiwyg_layer_paragraph_field_state($element, 'cwpf_flip_content_order', 'cwpf_enable_aside_content', 'visible', ["checked" => true]);
}

// sets field state in paragraph form
function _cwp_duo_wysiwyg_layer_paragraph_field_state(&$element, $field_name_to_hide, $field_name_condition, $state_key, array $conditions) {
  if (!isset($element['subform'][$field_name_to_hide])) {
    return;
  }

  $variation_field = $element['subform'][$field_name_condition];

  // construct variation input name to use it in #states
  $variation_input_name = array_shift($variation_field['widget']['#parents']);
  $variation_input_name .= '[' . implode('][', $variation_field['widget']['#parents']) . ']';
  $element['subform'][$field_name_to_hide]['#states'][$state_key][':input[name="' . $variation_input_name . '[value]"]'][] = $conditions;
}
